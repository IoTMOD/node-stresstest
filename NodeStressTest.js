const { composeAPI } = require('@iota/core')

const iota = composeAPI({
    provider: 'YOUR NODE ADDRESS HERE'
})


const seed = 'PUKGTPIHCKALPIPEWP99IBLDV9VRZRPMQZECREPCXYGYYEHCMZCDUUGYIXNPMBHRYFL9OMN9AAPEKZQZW' // dummy seed

const transfers = [{
    address: '999999999999999999999999999999999999999999999999999999999999999999999999999999999',
    value: 0,
    tag: 'POWSRV9IO9STESS9TEST',
    message: 'XBCDSCTCEAGDHDFDTCGDGDEAHDTCGDHDEAQCMDEAWCHDHDDDGDDBTATADDCDKDGDFDJDSAXCCD'
}]

const depth = 3 

const minWeightMagnitude = 14

// Set an intervall
setInterval(function(){  

iota.prepareTransfers(seed, transfers)
    .then(trytes => {
        return iota.sendTrytes(trytes, depth, minWeightMagnitude)
    })
    .then(bundle => {
        console.log(`Published transaction with tail hash: ${bundle[0].hash}`)
    })
    .catch(err => {
        console.log(err)
    })

}, 1000); // One transaction every 1s => stress with 1TPS
