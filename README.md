## How to start

- Clone or download this repo
- Change to the `node-stresstest` dir
- Edit the provider to your node's address
- Run the stresstest with `node NodeStressTest.js`